HTML TEST SPECIFICATION AND REQUIREMENTS

//Page overals
- Liquid layout
- max content wrapper width: 1280px, min widht: 800
- right column width: 30% of content wrapper
- left column and right column padding 10px
- page main title - embed font -> HelveticaInserat LT
- Logo element should be fixed at all time at the left border of the page

//Dynamics
- main navigation, drop down menu based on JS - don't use ready scripts
- right column dynamic boxes:
	* click to open, click to close
	* two boxes must not be open in the same time
	* don't use ready scripts
- Use the attached (content.js) JSON object to draw a table with its data

//Cross-browser
- IE8, 9
- FF
- Chrome

//Language menu
- hover makes flag opaque
- selected flag is opaque

//Misc
- font sizes and box sizes may be in %, px or em

//YOU MUST NOT USE!!!
- any ready scripts
- any JavaScript libraries
- bootstrap